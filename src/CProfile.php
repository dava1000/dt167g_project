<?php

declare(strict_types=1);

namespace myforum\private;

require_once __DIR__ . "/IHtmlOutput.php";
require_once __DIR__ . "/CDatabaseConn.php";
require_once __DIR__ . "/CPostForm.php";
require_once __DIR__ . "/CPostList.php";
require_once __DIR__ . "/CLoginManager.php";

class CProfile implements IHtmlOutput
{
    private string $uid;

    public function __construct(string $uid)
    {
        $this->uid = $uid;
    }

    public function putHtml()
    {
        // To let the user create new posts, only if they are in their own
        // profile page
        if (CLoginManager::isUserLoggedIn() && CLoginManager::getCurrentUser() === $this->uid) {
            $postForm = new CPostForm();
            $postForm->putHtml();
        }

        // The user posts are public

        $db = new CDatabaseConn();
        if ($db->open() === false) {
            echo "<h1>{$db->error}</h1>";
        } else {

            $rows = $db->getUserPosts($this->uid);
            if ($rows === false) {
                echo "<h1>{$db->error}</h1>";
            } else {
                $postList = new CPostList($rows);
                echo "<h1>{$this->uid}'s posts</h1>";
                $postList->putHtml();
            }
        }
    }
}
