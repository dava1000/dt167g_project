<?php

declare(strict_types=1);

namespace myforum\private;

require_once __DIR__ . "/CTokenManager.php";

class CSearchForm
{
    public function putHtml()
    {
        [$tk, $ts] = CTokenManager::generate();

        echo "
        <form action='/post/search' method='POST'>
            <fieldset>
                <legend>Search posts</legend>
                <input type='text' name='search-term' id='search-term' placeholder='substring ...' required>
                <input type='text' name='search-user' id='search-user' placeholder='username or empty ...'>
                <input type='submit' id='submit' value='Search'>
            </fieldset>
            <input type='hidden' name='tk' value='{$tk}' />
            <input type='hidden' name='ts' value='{$ts}' />
        </form>";
    }
}
