<?php

declare(strict_types=1);

namespace myforum\private;

require_once __DIR__ . "/CMessages.php";
require_once __DIR__ . "/CHeader.php";
require_once __DIR__ . "/CUserList.php";
require_once __DIR__ . "/CFooter.php";
require_once __DIR__ . "/CErrors.php";
require_once __DIR__ . "/CRouter.php";
require_once __DIR__ . "/CDatabaseConn.php";

class CUsersPage extends CMessages
{
    public function putHtml()
    {
        $header = new CHeader("MyForum - Users");
        $header->putHtml();

        echo "<main>";

        CMessages::putHtml();

        $db = new CDatabaseConn();

        if ($db->open() === false) {
            CRouter::getInstance()->redirect("/frontpage", ["error" => $db->error]);
            exit();
        }

        $users = $db->getUsers();
        if ($users === false) {
            CRouter::getInstance()->redirect("/frontpage", ["error" => $db->error]);
            exit();
        }

        echo "<h1>Users</h1>";
        $userList = new CUserList($users);
        $userList->putHtml();

        echo "</main>";

        $footer = new CFooter();
        $footer->putHtml();
    }
}
