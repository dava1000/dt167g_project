<?php

declare(strict_types=1);

namespace myforum\private;

require_once __DIR__ . "/IHtmlOutput.php";
require_once __DIR__ . "/CUser.php";

class CUserList implements IHtmlOutput
{
    private array $users;

    public function __construct(array $users)
    {
        $this->users = $users;
    }

    public function putHtml()
    {
        if (isset($this->users) && count($this->users) > 0) {

            echo "
            <table>
                <thead>
                    <tr>
                        <td>Username</td>
                        <td>Email</td>
                        <td>Registration date</td>
                    </tr>
                </thead>";

            echo "<tbody>";


            foreach ($this->users as $user) {
                $user->putHtml();
            }

            echo "</tbody>";
            echo "</table>";
        } else {
            echo "<h1>No users yet ...</h1>";
        }
    }
}
