<?php

declare(strict_types=1);

namespace myforum\private;

require_once __DIR__ . "/IHtmlOutput.php";

class CMessages implements IHtmlOutput
{
    private array $errors = [];
    private array $messages = [];

    public function addError(string $error)
    {
        array_push($this->errors, $error);
    }

    public function addMessage(string $message)
    {
        array_push($this->messages, $message);
    }

    private function putErrors()
    {
        if (count($this->errors)) {
            echo "<section>";
            echo "<h1>Errors</h1>";
            foreach ($this->errors as $error) {
                echo "<p>{$error}</p>";
            }
            echo "</section>";
        }
    }

    private function putMessages()
    {
        if (count($this->messages)) {
            echo "<section>";
            echo "<h1>Messages</h1>";
            foreach ($this->messages as $message) {
                echo "<p>{$message}</p>";
            }
            echo "</section>";
        }
    }

    public function putHtml()
    {
        $this->putErrors();
        $this->putMessages();
    }
}
