<?php

declare(strict_types=1);

namespace myforum\private;

require_once __DIR__ . "/IHtmlOutput.php";
require_once __DIR__ . "/CTokenManager.php";
require_once __DIR__ . "/CLoginManager.php";
require_once __DIR__ . "/CPost.php";

class CPostActions implements IHtmlOutput
{
    private CPost $post;

    public function __construct(CPost &$post)
    {
        $this->post = $post;
    }

    public function putHtml()
    {
        if (CLoginManager::isUserLoggedIn()) {

            [$tk, $ts] = CTokenManager::generate();
            $uid = CLoginManager::getCurrentUser();

            echo "
            <form action='/post/upvote' method='POST'>
                <fieldset>
                    <button type='submit' id='upvote-{$this->post->postid}'>
                        <i class='font-icon green'>&#xf164</i>
                    </button>
                    <label class='green' for='upvote-{$this->post->postid}'>{$this->post->upvotes}</label>
                </fieldset>
                <input type='hidden' name='postid' value={$this->post->postid} />
                <input type='hidden' name='uid' value='{$uid}' />
                <input type='hidden' name='tk' value='{$tk}' />
                <input type='hidden' name='ts' value='{$ts}' />
            </form>
            <form action='/post/downvote' method='POST'>
                <fieldset>
                    <button type='submit' id='downvote-{$this->post->postid}'>
                        <i class='font-icon red'>&#xf165</i>
                    </button>
                    <label class='red' for='downvote-{$this->post->postid}'>{$this->post->downvotes}</label>
                </fieldset>
                <input type='hidden' name='postid' value={$this->post->postid} />
                <input type='hidden' name='uid' value='{$uid}' />
                <input type='hidden' name='tk' value='{$tk}' />
                <input type='hidden' name='ts' value='{$ts}' />
            </form>";

            // One can only delete it own posts
            if (CLoginManager::getCurrentUser() === $this->post->user) {
                echo "
                <form action='/post/delete' method='POST'>
                    <fieldset>
                        <button type='submit' id='delete-{$this->post->postid}'>
                            <i class='font-icon red'>&#xf1f8</i>
                        </button>
                    </fieldset>
                    <input type='hidden' name='postid' value={$this->post->postid} />
                    <input type='hidden' name='uid' value='{$uid}' />
                    <input type='hidden' name='tk' value='{$tk}' />
                    <input type='hidden' name='ts' value='{$ts}' />
                </form>";
            }
        } else {
            echo "
            <span class='green'>{$this->post->upvotes}</span>
            <span class='red'>{$this->post->downvotes}</span>";
        }
    }
}
