<?php

declare(strict_types=1);

namespace myforum\private;

require_once __DIR__ . "/CMessages.php";
require_once __DIR__ . "/CHeader.php";
require_once __DIR__ . "/CFooter.php";
require_once __DIR__ . "/CProfile.php";

class CProfilePage extends CMessages
{
    private string $uid;

    public function __construct(string $uid)
    {
        $this->uid = $uid;
    }

    public function putHtml()
    {
        $header = new CHeader("MyForum - Profile");
        $header->putHtml();

        echo "<main>";

        CMessages::putHtml();

        $profile = new CProfile($this->uid);
        $profile->putHtml();

        echo "</main>";

        $footer = new CFooter();
        $footer->putHtml();
    }
}
