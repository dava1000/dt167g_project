<?php

declare(strict_types=1);

namespace myforum\private;

require_once __DIR__ . "/IHtmlOutput.php";
require_once __DIR__ . "/CLoginManager.php";

class CHeader implements IHtmlOutput
{
    private string $pageTitle;

    public function __construct(string $pageTitle = "MyForum")
    {
        $this->pageTitle = $pageTitle;
    }

    public function putHtml()
    {
        $frontPageUrl = "/frontpage";
        $searchPageUrl = "/post/search";
        $usersPageUrl = "/users";
        $loginPageUrl = "/user/login";
        $signupPageUrl = "/user/add";

        echo "
        <!DOCTYPE html>
        <html lang='en'>

        <head>
        <meta charset='UTF-8'>
        <meta http-equiv='X-UA-Compatible' content='IE=edge'>
        <meta name='viewport' content='width=device-width, initial-scale=1.0'>
        <title>{$this->pageTitle}</title>
        <link rel='stylesheet' href='/css/style.css'>
        </head>

        <body>

        <header>
            <a href='{$frontPageUrl}'><i class='font-icon'>&#xe800</i> MyForum</a>
            <nav>
                <a href='{$searchPageUrl}'><i class='font-icon'>&#xe807</i> Search</a>
                <a href='{$usersPageUrl}'><i class='font-icon'>&#xe804</i> Users</a>";

        if (CLoginManager::isUserLoggedIn()) {
            $uid = CLoginManager::getCurrentUser();
            [$tk, $ts] = CTokenManager::generate();

            $profilePageUrl = "/user/profile?uid={$uid}";

            echo "<a href='{$profilePageUrl}'><i class='font-icon'>&#xe805</i> " . CLoginManager::getCurrentUser() . "</a>";

            echo "
            <form action='/user/logout' method='POST'>
                <fieldset>
                    <button type='submit'><i class='font-icon'>&#xe806</i> Logout</button>
                </fieldset>
                <input type='hidden' name='uid' value='{$uid}' />
                <input type='hidden' name='tk' value='{$tk}' />
                <input type='hidden' name='ts' value='{$ts}' />
            </form>";
        } else {
            echo "<a href='{$loginPageUrl}'><i class='font-icon'>&#xe803</i> Login</a>";
            echo "<a href='{$signupPageUrl}'><i class='font-icon'>&#xf234</i> Signup</a>";
        }

        echo "</nav>";
        echo "</header>";
    }
}
