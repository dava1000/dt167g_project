<?php

declare(strict_types=1);

namespace myforum\private;

require_once __DIR__ . "/CMessages.php";
require_once __DIR__ . "/CHeader.php";
require_once __DIR__ . "/CFooter.php";
require_once __DIR__ . "/CSignupForm.php";

class CSignupPage extends CMessages
{
    public function putHtml()
    {
        $header = new CHeader("MyForum - Signup");
        $header->putHtml();

        echo "<main>";

        CMessages::putHtml();

        $signupForm = new CSignupForm();
        $signupForm->putHtml();

        echo "</main>";

        $footer = new CFooter();
        $footer->putHtml();
    }
}
