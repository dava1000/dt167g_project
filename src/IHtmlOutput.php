<?php

declare(strict_types=1);

namespace myforum\private;

interface IHtmlOutput
{
    public function putHtml();
}
