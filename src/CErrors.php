<?php

namespace myforum\private;

class CErrors
{
    public static string $err_missing_tokens = "Missing Anti-CSRF tokens";
    public static string $err_bad_tokens = "Anti-CSRF tokens are tampered";

    public static string $err_no_user_logged_in = "No user is logged in";

    public static string $err_missing_params = "One or more params are missing";
    public static string $err_missing_uid = "Username is missing";
    public static string $err_missing_email = "User email is missing";
    public static string $err_missing_pwd = "User password is missing";
    public static string $err_missing_pwd_rep = "User password repeat is missing";
    public static string $err_pwds_differ = "The passwords entered differ";
    public static string $err_email_malformed = "Bad email format";
    public static string $err_param_sanitazion = "Parameter contains invalid characters";
    public static string $err_invalid_operation = "Invalid operation";

    public static string $err_database_generic = "Database operation failed";
    public static string $err_database_connection = "Failed to connect to database";
    public static string $err_database_authentication = "Failed to authenticate user";
    public static string $err_database_password_hashing = "Failed to create password hash";
    public static string $err_database_create_user = "Failed to create user account";
    public static string $err_database_get_users = "Failed to retrieve users from database";
    public static string $err_database_get_user_posts = "Failed to retrieve user posts from database";
    public static string $err_database_get_posts = "Failed to retrieve posts from database";
    public static string $err_database_create_post = "Failed to create user post";
    public static string $err_database_delete_post = "Failed to delete user post";
    public static string $err_database_get_post = "Failed to find post";
    public static string $err_database_upvote_post = "You already upvoted this post";
    public static string $err_database_downvote_post = "You already downvoted this post";
    public static string $err_database_get_post_votes = "Could not fetch post votes";
}
