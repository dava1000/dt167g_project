<?php

declare(strict_types=1);

namespace myforum\private;

require_once __DIR__ . "/IHtmlOutput.php";
require_once __DIR__ . "/CPostActions.php";

class CPost implements IHtmlOutput
{
    public int $postid;
    public string $user;
    public string $date;
    public string $title;
    public string $content;
    public mixed $parent;
    public int $downvotes;
    public int $upvotes;

    public function __construct(
        int $postid,
        string $user,
        string $date,
        string $title,
        string $content,
        int $downvotes,
        int $upvotes,
        mixed $parent = null,
    ) {
        $this->postid = $postid;
        $this->user = $user;
        $this->date = $date;
        $this->title = $title;
        $this->content = $content;
        $this->downvotes = $downvotes;
        $this->upvotes = $upvotes;
        $this->parent = $parent;
    }

    public function putHtml()
    {
        $profilePageUrl = "/user/profile?uid={$this->user}";

        echo "
        <section class='post'>
            <span>#{$this->postid}</span>
            <span>\"{$this->title}\"</span>
            <a href='{$profilePageUrl}'>{$this->user}</a>
            <span>{$this->date}</span>
            <p>{$this->content}</p>";

        $postActions = new CPostActions($this);
        $postActions->putHtml();

        echo "</section>";
    }
}
