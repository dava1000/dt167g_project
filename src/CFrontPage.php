<?php

declare(strict_types=1);

namespace myforum\private;

require_once __DIR__ . "/CMessages.php";
require_once __DIR__ . "/CHeader.php";
require_once __DIR__ . "/CFooter.php";
require_once __DIR__ . "/CDatabaseConn.php";
require_once __DIR__ . "/CRouter.php";
require_once __DIR__ . "/CPostList.php";

class CFrontPage extends CMessages
{
    public function putHtml()
    {
        $header = new CHeader("MyForum - Frontpage");
        $header->putHtml();

        echo "<main>";

        CMessages::putHtml();

        echo "<h1>Front page ...</h1>";

        $db = new CDatabaseConn();
        if ($db->open() === false) {
            CRouter::getInstance()->redirect("/frontpage", ["error" => $db->error]);
            exit();
        }

        $posts = $db->getPosts();
        if ($posts === false) {
            CRouter::getInstance()->redirect("/frontpage", ["error" => $db->error]);
            exit();
        }

        $postList = new CPostList($posts);
        $postList->putHtml();

        echo "</main>";

        $footer = new CFooter();
        $footer->putHtml();
    }
}
