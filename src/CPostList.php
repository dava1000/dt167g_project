<?php

declare(strict_types=1);

namespace myforum\private;

require_once __DIR__ . "/IHtmlOutput.php";
require_once __DIR__ . "/CPost.php";

class CPostList implements IHtmlOutput
{
    private array $posts;

    public function __construct(array $posts)
    {
        $this->posts = $posts;
    }

    public function putHtml()
    {
        if (isset($this->posts) && count($this->posts) > 0) {
            foreach ($this->posts as $post) {
                $post->putHtml();
            }
        } else {
            echo "<h1>No posts made yet ...</h1>";
        }
    }
}
