<?php

declare(strict_types=1);

namespace myforum\private;

require_once __DIR__ . "/IHtmlOutput.php";
require_once __DIR__ . "/CTokenManager.php";

class CPostForm implements IHtmlOutput
{
    private string $legend;
    private int $initialCols;
    private int $initialRows;

    public function __construct(string $legend = "New post", int $cols = 80, int $rows = 2)
    {
        $this->legend = $legend;
        $this->initialCols = $cols;
        $this->initialRows = $rows;
    }

    public function putHtml()
    {
        [$tk, $ts] = CTokenManager::generate();

        echo "
        <form action='/post/add' name='postform' id='postform' method='POST'>
            <fieldset>
                <legend>{$this->legend}</legend>
                <input type='text' name='title' id='title' placeholder='title ...' required>
                <textarea id='content'
                    cols='{$this->initialCols}' rows ='{$this->initialRows}'
                    name='content' form='postform' placeholder='write contents here ...' required></textarea>
                <input type='submit' name='submit' value='save'>
            </fieldset>
            <input type='hidden' name='tk' value='{$tk}' />
            <input type='hidden' name='ts' value='{$ts}' />
        </form>";
    }
}
