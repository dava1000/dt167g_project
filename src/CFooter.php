<?php

declare(strict_types=1);

namespace myforum\private;

require_once __DIR__ . "/IHtmlOutput.php";

class CFooter implements IHtmlOutput
{
    private string $email;

    public function __construct(string $email = "dava1000@student.miun.se")
    {
        $this->email = $email;
    }

    public function putHtml()
    {
        echo "
                <footer>
                    <section>
                        <h2>Contact info</h2>
                        <a href='mailto:{$this->email}'>{$this->email}</a>
                    </section>
                </footer>
            </body>
        </html>";
    }
}