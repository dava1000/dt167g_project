<?php

declare(strict_types=1);

namespace myforum\private;

session_start();

// Simple session based login managment
class CLoginManager
{
    public static function loginUser(string $uid)
    {
        $_SESSION["uid"] = $uid;
    }

    public static function logoutUser()
    {
        // Remove session variables
        session_unset();

        // Remove the cookie
        $cookie_params = session_get_cookie_params();
        setcookie(
            session_name(),
            "",
            time() - 86400,
            $cookie_params["path"],
            $cookie_params["domain"],
            $cookie_params["secure"],
            $cookie_params["httponly"]
        );

        // Nuke the session
        session_destroy();
    }

    public static function getCurrentUser(): string | null
    {
        if (isset($_SESSION["uid"])) {
            return $_SESSION["uid"];
        }

        return null;
    }

    public static function isUserLoggedIn(): bool
    {
        return isset($_SESSION["uid"]);
    }
}
