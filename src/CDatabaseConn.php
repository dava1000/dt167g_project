<?php

declare(strict_types=1);

namespace myforum\private;

use mysqli;
use mysqli_sql_exception;

require_once __DIR__ . "/CErrors.php";
require_once __DIR__ . "/CPost.php";
require_once __DIR__ . "/CUser.php";
require_once __DIR__ . "/../config/config.php";

class CDatabaseConn
{
    public mysqli $conn;
    public string $error;

    public function __construct()
    {
        unset($this->conn);
        $this->error = "";
    }

    public function __destruct()
    {
        $this->close();
    }

    public function open(): bool
    {
        if (isset($this->conn)) {
            return true;
        }

        try {
            global $dbconfig;
            mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
            $this->conn = new mysqli(
                $dbconfig["DB_HOST"],
                $dbconfig["DB_USER"],
                $dbconfig["DB_PWD"],
                $dbconfig["DB_DB"],
                $dbconfig["DB_PORT"]
            );
            $this->conn->set_charset("utf8mb4");
        } catch (mysqli_sql_exception $e) {
            global $env, $env_debug;
            if ($env === $env_debug) {
                $this->error = $e->getMessage();
            } else {
                $this->error = CErrors::$err_database_connection;
            }
            return false;
        }

        return true;
    }

    public function close()
    {
        if (isset($this->conn)) {
            $this->conn->close();
            unset($this->conn);
        }
    }

    function validateCredentials(string $uid, string $pwd): bool
    {
        $query = "SELECT password FROM User WHERE username=? LIMIT 1;";

        try {
            $stmt = $this->conn->stmt_init();
            $stmt->prepare($query);
            $stmt->bind_param("s", $uid);
            $stmt->execute();

            $result = $stmt->get_result();
            if ($result === false) {
                // No result set
                $this->error = CErrors::$err_database_authentication;
                return false;
            }

            $row = $result->fetch_assoc();
            if (is_null($row) || count($row) === 0) {
                // The username was not found
                $this->error = CErrors::$err_database_authentication;
                return false;
            }

            if (password_verify($pwd, $row["password"]) === false) {
                // The passwords don't match
                $this->error = CErrors::$err_database_authentication;
                return false;
            }

            $result->free();
            return true;
        } catch (mysqli_sql_exception $e) {
            global $env, $env_debug;
            if ($env === $env_debug) {
                $this->error = $e->getMessage();
            } else {
                $this->error = CErrors::$err_database_generic;
            }
            return false;
        } finally {
            if (isset($stmt)) {
                $stmt->close();
            }
        }
    }

    public function createUser($uid, $pwd, $email): bool
    {
        $hash = password_hash($pwd, PASSWORD_DEFAULT);
        if (is_null($hash) || $hash === false) {
            $this->error = CErrors::$err_database_password_hashing;
            return false;
        }

        $query = "INSERT INTO User(username,password,email) VALUES(?,?,?);";

        try {
            $stmt = $this->conn->stmt_init();
            $stmt->prepare($query);
            $stmt->bind_param("sss", $uid, $hash, $email);
            $stmt->execute();

            if ($stmt->affected_rows === 0) {
                // No new user created
                $this->error = CErrors::$err_database_create_user;
                return false;
            }

            return true;
        } catch (mysqli_sql_exception $e) {
            global $env, $env_debug;
            if ($env === $env_debug) {
                $this->error = $e->getMessage();
            } else {
                $this->error = CErrors::$err_database_generic;
            }
            return false;
        } finally {
            if (isset($stmt)) {
                $stmt->close();
            }
        }
    }

    function getUsers(): array | false
    {
        $query = "SELECT username,email,reg_date FROM User ORDER BY username ASC;";

        try {
            $result = $this->conn->query($query);
            if ($result === false) {
                $this->error = CErrors::$err_database_get_users;
                return false;
            }

            $rows = [];
            while ($row = $result->fetch_assoc()) {
                $user = new CUser($row["username"], $row["email"], $row["reg_date"]);
                array_push($rows, $user);
            }

            $result->free();
            return $rows;
        } catch (mysqli_sql_exception $e) {
            global $env, $env_debug;
            if ($env === $env_debug) {
                $this->error = $e->getMessage();
            } else {
                $this->error = CErrors::$err_database_generic;
            }
            return false;
        }
    }

    function isUser(string $uid): bool
    {
        $query = "SELECT * FROM User WHERE username=? LIMIT 1;";

        try {
            $stmt = $this->conn->stmt_init();
            $stmt->prepare($query);
            $stmt->bind_param("s", $uid);
            $stmt->execute();

            $result = $stmt->get_result();
            if ($result === false) {
                $this->error = CErrors::$err_database_get_users;
                return false;
            }

            $row = $result->fetch_assoc();
            $result->free();

            if (is_null($row) || count($row) == 0) {
                return false;
            }

            return $row["username"] === $uid;
        } catch (mysqli_sql_exception $e) {
            global $env, $env_debug;
            if ($env === $env_debug) {
                $this->error = $e->getMessage();
            } else {
                $this->error = CErrors::$err_database_generic;
            }
            return false;
        } finally {
            if (isset($stmt)) {
                $stmt->close();
            }
        }
    }

    function getUserPosts(string $uid): array | false
    {
        $query = "
        SELECT p.*,
            (SELECT COUNT(v.vote)
            FROM Voting AS v
            WHERE v.vote < 0 and v.post=p.id) downvotes,
            (SELECT COUNT(v.vote)
            FROM Voting AS v
            WHERE v.vote > 0 and v.post=p.id) upvotes
        FROM Post AS p
        WHERE p.user=?
        ORDER BY date DESC;";

        try {
            $stmt = $this->conn->stmt_init();
            $stmt->prepare($query);
            $stmt->bind_param("s", $uid);
            $stmt->execute();

            $result = $stmt->get_result();
            if ($result === false) {
                $this->error = CErrors::$err_database_get_user_posts;
                return false;
            }

            $rows = [];
            while ($row = $result->fetch_assoc()) {
                $post = new CPost(
                    $row["id"],
                    $row["user"],
                    $row["date"],
                    $row["title"],
                    $row["content"],
                    $row["downvotes"],
                    $row["upvotes"],
                    $row["parent"]
                );

                array_push($rows, $post);
            }

            $result->free();
            return $rows;
        } catch (mysqli_sql_exception $e) {
            global $env, $env_debug;
            if ($env === $env_debug) {
                $this->error = $e->getMessage();
            } else {
                $this->error = CErrors::$err_database_generic;
            }
            return false;
        } finally {
            if (isset($stmt)) {
                $stmt->close();
            }
        }
    }

    function getPosts(): array | false
    {
        $query = "
        SELECT p.*,
            (SELECT COUNT(v.vote)
            FROM Voting AS v
            WHERE v.vote < 0 and v.post=p.id) downvotes,
            (SELECT COUNT(v.vote)
            FROM Voting AS v
            WHERE v.vote > 0 and v.post=p.id) upvotes
        FROM Post AS p
        ORDER BY date DESC;";

        try {
            $result = $this->conn->query($query);
            if ($result === false) {
                $this->error = CErrors::$err_database_get_posts;
                return false;
            }

            $rows = [];
            while ($row = $result->fetch_assoc()) {
                $post = new CPost(
                    (int) $row["id"],
                    $row["user"],
                    $row["date"],
                    $row["title"],
                    $row["content"],
                    (int) $row["downvotes"],
                    (int) $row["upvotes"],
                    $row["parent"]
                );

                array_push($rows, $post);
            }

            $result->free();
            return $rows;
        } catch (mysqli_sql_exception $e) {
            global $env, $env_debug;
            if ($env === $env_debug) {
                $this->error = $e->getMessage();
            } else {
                $this->error = CErrors::$err_database_generic;
            }
            return false;
        }
    }

    public function createPost($uid, $title, $content, $parent): bool
    {
        $query = "INSERT INTO Post(user,title,content,parent) VALUES(?,?,?,?);";

        try {
            $stmt = $this->conn->stmt_init();
            $stmt->prepare($query);
            $stmt->bind_param("sssi", $uid, $title, $content, $parent);
            $stmt->execute();

            if ($stmt->affected_rows === 0) {
                // No new post created
                $this->error = CErrors::$err_database_create_post;
                return false;
            }

            return true;
        } catch (mysqli_sql_exception $e) {
            global $env, $env_debug;
            if ($env === $env_debug) {
                $this->error = $e->getMessage();
            } else {
                $this->error = CErrors::$err_database_generic;
            }
            return false;
        } finally {
            if (isset($stmt)) {
                $stmt->close();
            }
        }
    }

    public function deletePost($postid, $uid): bool
    {
        $query = "DELETE FROM Post WHERE id=? AND user=?;";

        try {
            $stmt = $this->conn->stmt_init();
            $stmt->prepare($query);
            $stmt->bind_param("ss", $postid, $uid);
            $stmt->execute();

            if ($stmt->affected_rows === 0) {
                // Post not deleted
                $this->error = CErrors::$err_database_delete_post;
                return false;
            }

            return true;
        } catch (mysqli_sql_exception $e) {
            global $env, $env_debug;
            if ($env === $env_debug) {
                $this->error = $e->getMessage();
            } else {
                $this->error = CErrors::$err_database_generic;
            }
            return false;
        } finally {
            if (isset($stmt)) {
                $stmt->close();
            }
        }
    }

    function findPosts(string $substr): array | false
    {
        $query = "
        SELECT p.*,
            (SELECT COUNT(v.vote)
            FROM Voting AS v
            WHERE v.vote < 0 and v.post=p.id) downvotes,
            (SELECT COUNT(v.vote)
            FROM Voting AS v
            WHERE v.vote > 0 and v.post=p.id) upvotes
        FROM Post AS p
        WHERE p.content LIKE ?
        ORDER BY date DESC;";

        try {
            $stmt = $this->conn->stmt_init();
            $stmt->prepare($query);

            $substr = "%{$substr}%";
            $stmt->bind_param("s", $substr);
            $stmt->execute();

            $result = $stmt->get_result();
            if ($result === false) {
                // No result set
                $this->error = CErrors::$err_database_get_post;
                return false;
            }

            $rows = [];
            while ($row = $result->fetch_assoc()) {
                $post = new CPost(
                    $row["id"],
                    $row["user"],
                    $row["date"],
                    $row["title"],
                    $row["content"],
                    $row["downvotes"],
                    $row["upvotes"],
                    $row["parent"]
                );

                array_push($rows, $post);
            }

            $result->free();
            return $rows;
        } catch (mysqli_sql_exception $e) {
            global $env, $env_debug;
            if ($env === $env_debug) {
                $this->error = $e->getMessage();
            } else {
                $this->error = CErrors::$err_database_generic;
            }
            return false;
        } finally {
            if (isset($stmt)) {
                $stmt->close();
            }
        }
    }

    function findPostsByUser(string $substr, string $uid): array | false
    {
        $query = "
        SELECT p.*,
            (SELECT COUNT(v.vote)
            FROM Voting AS v
            WHERE v.vote < 0 and v.post=p.id) downvotes,
            (SELECT COUNT(v.vote)
            FROM Voting AS v
            WHERE v.vote > 0 and v.post=p.id) upvotes
        FROM Post AS p
        WHERE p.content LIKE ? AND p.user=?
        ORDER BY date DESC;";

        try {
            $stmt = $this->conn->stmt_init();
            $stmt->prepare($query);

            $substr = "%{$substr}%";
            $stmt->bind_param("ss", $substr, $uid);
            $stmt->execute();

            $result = $stmt->get_result();
            if ($result === false) {
                // No result set
                $this->error = CErrors::$err_database_get_post;
                return false;
            }

            $rows = [];
            while ($row = $result->fetch_assoc()) {
                $post = new CPost(
                    $row["id"],
                    $row["user"],
                    $row["date"],
                    $row["title"],
                    $row["content"],
                    $row["downvotes"],
                    $row["upvotes"],
                    $row["parent"]
                );

                array_push($rows, $post);
            }

            $result->free();
            return $rows;
        } catch (mysqli_sql_exception $e) {
            global $env, $env_debug;
            if ($env === $env_debug) {
                $this->error = $e->getMessage();
            } else {
                $this->error = CErrors::$err_database_generic;
            }
            return false;
        } finally {
            if (isset($stmt)) {
                $stmt->close();
            }
        }
    }

    private function registerUserVoting(string $uid, int $postid): bool
    {
        // Query to make sure an entry is made in the voting table
        // Before the voting itself is made
        $query = "
        INSERT INTO Voting (user, post) VALUES (?, ?)
        ON DUPLICATE KEY UPDATE user=?, post=?;";

        try {
            $stmt = $this->conn->stmt_init();
            $stmt->prepare($query);
            $stmt->bind_param("sisi", $uid, $postid, $uid, $postid);
            $stmt->execute();
            return true;
        } catch (mysqli_sql_exception $e) {
            global $env, $env_debug;
            if ($env === $env_debug) {
                $this->error = $e->getMessage();
            } else {
                $this->error = CErrors::$err_database_generic;
            }
            return false;
        } finally {
            if (isset($stmt)) {
                $stmt->close();
            }
        }
    }

    public function upvotePost(string $uid, int $postid): bool
    {
        if (!$this->registerUserVoting($uid, $postid)) {
            return false;
        }

        $query = "
        UPDATE
            Voting v
        SET
            v.vote = v.vote + 1
        WHERE
            v.post = ?
        AND
            v.user = ?
        AND
            v.vote < 1;
        ";

        try {
            $stmt = $this->conn->stmt_init();
            $stmt->prepare($query);
            $stmt->bind_param("is", $postid, $uid);
            $stmt->execute();

            if ($stmt->affected_rows === 0) {
                // The voting could not be updated because it is already at +1
                $this->error = CErrors::$err_database_upvote_post;
                return false;
            }

            return true;
        } catch (mysqli_sql_exception $e) {
            global $env, $env_debug;
            if ($env === $env_debug) {
                $this->error = $e->getMessage();
            } else {
                $this->error = CErrors::$err_database_generic;
            }
            return false;
        } finally {
            if (isset($stmt)) {
                $stmt->close();
            }
        }
    }

    public function downvotePost(string $uid, int $postid): bool
    {
        if (!$this->registerUserVoting($uid, $postid)) {
            return false;
        }

        $query = "
        UPDATE
            Voting v
        SET
            v.vote = v.vote - 1
        WHERE
            v.post = ?
        AND
            v.user = ?
        AND
            v.vote > -1;
        ";

        try {
            $stmt = $this->conn->stmt_init();
            $stmt->prepare($query);
            $stmt->bind_param("is", $postid, $uid);
            $stmt->execute();

            if ($stmt->affected_rows === 0) {
                // The voting could not be downvoted because it is already at -1
                $this->error = CErrors::$err_database_downvote_post;
                return false;
            }

            return true;
        } catch (mysqli_sql_exception $e) {
            global $env, $env_debug;
            if ($env === $env_debug) {
                $this->error = $e->getMessage();
            } else {
                $this->error = CErrors::$err_database_generic;
            }
            return false;
        } finally {
            if (isset($stmt)) {
                $stmt->close();
            }
        }
    }

    public function getPostVotes(int $postid)
    {
        $query = "
        SELECT *
        FROM
            (SELECT COUNT(vote) AS downvotes
            FROM Voting
            WHERE vote < 0 and post=?) t1,
            (SELECT COUNT(vote) AS upvotes
            FROM Voting
            WHERE vote > 0 and post=?) t2;
        ";

        try {
            $stmt = $this->conn->stmt_init();
            $stmt->prepare($query);
            $stmt->bind_param("ii", $postid, $postid);
            $stmt->execute();

            $result = $stmt->get_result();
            if ($result === false) {
                // No result set
                $this->error = CErrors::$err_database_get_post_votes;
                return false;
            }

            $rows = $result->fetch_assoc();
            $result->free();

            return $rows;
        } catch (mysqli_sql_exception $e) {
            global $env, $env_debug;
            if ($env === $env_debug) {
                $this->error = $e->getMessage();
            } else {
                $this->error = CErrors::$err_database_generic;
            }
            return false;
        } finally {
            if (isset($stmt)) {
                $stmt->close();
            }
        }
    }
}
