<?php

declare(strict_types=1);

namespace myforum\private;

use DateTime;

class CTokenManager
{
    public static function generate(): array
    {
        // Get a timestamp for the new token
        $date = new DateTime();
        $ts = (string)$date->getTimestamp();

        $tk = self::createToken($ts);

        return [$tk, $ts];
    }

    public static function validate(string $tk, string $ts): bool
    {
        session_start();

        // No token have been generated previosly
        if (!isset($_SESSION["token"])) {
            return false;
        }

        // The token and timestamp need to contain actual non-blank strings
        if (!isset($tk) || !isset($ts) || empty(trim($tk)) || empty(trim($ts))) {
            return false;
        }

        $realToken = self::createToken($ts);

        // Only true if the token was created using the same method as
        // the one in generate_token
        return $tk === $realToken;
    }

    private static function createToken($ts): string
    {
        session_start();

        // Create session token if it does not exist
        if (!isset($_SESSION["token"])) {
            $_SESSION["token"] = bin2hex(random_bytes(20));
        }

        // For the site_secret
        require_once __DIR__ . "/../config/config.php";
        global $site_secret;

        // Generate a 40-character hex number using the site secret, session
        // token and the token timestamp
        $tk = sha1($_SESSION["token"] . session_id() . $site_secret . $ts);

        return $tk;
    }
}
