<?php

declare(strict_types=1);

namespace myforum\private;

require_once __DIR__ . "/IHtmlOutput.php";

class CUser implements IHtmlOutput
{
    public string $username;
    public string $email;
    public string $reg_date;

    public function __construct(string $username, string $email, string $reg_date)
    {
        $this->username = $username;
        $this->email = $email;
        $this->reg_date = $reg_date;
    }

    public function putHtml()
    {
        $profilePageUrl = "/user/profile?uid={$this->username}";

        echo "
        <tr>
            <td><a href='{$profilePageUrl}'>{$this->username}</a></td>
            <td><a href='mailto:{$this->email}'>{$this->email}</a></td>
            <td>{$this->reg_date}</td>
        </tr>
        ";
    }
}
