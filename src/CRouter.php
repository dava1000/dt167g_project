<?php

declare(strict_types=1);

namespace myforum\private;

require_once __DIR__ . "/CTokenManager.php";

class CRouter
{
    private $get_routes = [];
    private $post_routes = [];

    private static $instance;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (!isset(CRouter::$instance)) {
            CRouter::$instance = new CRouter();
        }
        return CRouter::$instance;
    }

    public function get(string $path, $handler)
    {
        $this->get_routes[$path] = $handler;
    }

    public function post(string $path, $handler)
    {
        $this->post_routes[$path] = $handler;
    }

    public function redirect(string $path, $params = [])
    {
        $url = $path;

        if (isset($this->get_routes[$path])) {
            if (count($params) > 0) {
                $url = "{$url}?";
                foreach ($params as $key => $value) {
                    $url = "{$url}{$key}={$value}&";
                }

                $url = rtrim($url, "&");
            }

            header("Location: {$url}");
            exit();
        } else {
            header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found");
            exit();
        }
    }

    public function resolve()
    {
        $uri = (string)$_SERVER["REQUEST_URI"];
        $method = (string)$_SERVER["REQUEST_METHOD"];

        // Get the base uri without params or tokens
        if (mb_substr_count($uri, "?") > 0) {
            $uri = mb_substr($uri, 0, mb_strpos($uri, "?"));
        }

        // Remove trailing slash, if any and the slash is not root
        if (mb_substr_count($uri, "/") > 1 && mb_substr($uri, -1) === "/") {
            $uri = mb_substr($uri, 0, -1);
        }

        if ($method === "GET") {
            if (isset($this->get_routes[$uri])) {
                $handler = $this->get_routes[$uri];
                call_user_func($handler, $_GET);
            } else {
                header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found");
                exit();
            }
        } elseif ($method === "POST") {
            if (isset($this->post_routes[$uri])) {
                if (!isset($_POST["tk"]) || !isset($_POST["ts"])) {
                    $this->redirect("/", ["error" => CErrors::$err_missing_tokens]);
                    exit();
                }

                if (!CTokenManager::validate($_POST["tk"], $_POST["ts"])) {
                    $this->redirect("/", ["error" => CErrors::$err_bad_tokens]);
                    exit();
                }

                $handler = $this->post_routes[$uri];
                call_user_func($handler, $_POST);
            } else {
                header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found");
                exit();
            }
        } else {
            header($_SERVER["SERVER_PROTOCOL"] . "HTTP/1.0 405 Method Not Allowed");
            exit();
        }
    }

    public function dump()
    {
        print_r($this->get_routes);
        print_r($this->post_routes);
    }
}
