<?php

declare(strict_types=1);

namespace myforum\private;

require_once __DIR__ . "/CRouter.php";
require_once __DIR__ . "/CErrors.php";
require_once __DIR__ . "/CLoginManager.php";
require_once __DIR__ . "/CFrontPage.php";
require_once __DIR__ . "/CLoginPage.php";
require_once __DIR__ . "/CSignupPage.php";
require_once __DIR__ . "/CProfilePage.php";
require_once __DIR__ . "/CUsersPage.php";
require_once __DIR__ . "/CSearchPage.php";
require_once __DIR__ . "/CSearchResultsPage.php";
require_once __DIR__ . "/CDatabaseConn.php";

$router = CRouter::getInstance();

// --------------------------------------------------------------------
// GET
// --------------------------------------------------------------------

function addMessagesToPageIfExists($page, $in)
{
    if (isset($in["error"]) && !empty(trim($in["error"]))) {
        $error = filter_var(trim($in["error"]), FILTER_SANITIZE_STRING);
        if ($error) {
            $page->addError($error);
        }
    }

    if (isset($in["msg"]) && !empty(trim($in["msg"]))) {
        $msg = filter_var(trim($in["msg"]), FILTER_SANITIZE_STRING);
        if ($msg) {
            $page->addMessage($msg);
        }
    }
}

$router->get("/", function ($in) {
    global $router;
    $router->redirect("/frontpage");
});

$router->get("/frontpage", function ($in) {
    $frontPage = new CFrontPage();
    addMessagesToPageIfExists($frontPage, $in);
    $frontPage->putHtml();
});

$router->get("/user/login", function ($in) {
    $loginPage = new CLoginPage();
    addMessagesToPageIfExists($loginPage, $in);
    $loginPage->putHtml();
});

$router->get("/user/add", function ($in) {
    $signupPage = new CSignupPage();
    addMessagesToPageIfExists($signupPage, $in);
    $signupPage->putHtml();
});

$router->get("/users", function ($in) {
    $usersPage = new CUsersPage();
    addMessagesToPageIfExists($usersPage, $in);
    $usersPage->putHtml();
});

$router->get("/user/profile", function ($in) {
    global $router;

    if (!isset($in["uid"]) || empty(trim($in["uid"]))) {
        $router->redirect("/frontpage", ["error" => CErrors::$err_missing_uid]);
        exit();
    }

    $uid = filter_var(trim($in["uid"]), FILTER_SANITIZE_STRING);
    if ($uid === false) {
        $router->redirect("/frontpage", ["error" => CErrors::$err_param_sanitazion]);
        exit();
    }

    $db = new CDatabaseConn();
    if (!$db->open()) {
        $router->redirect("/frontpage", ["error" => $db->error]);
        exit();
    }

    if (!$db->isUser($uid)) {
        header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found");
        exit();
    }

    $profilePage = new CProfilePage($uid);
    addMessagesToPageIfExists($profilePage, $in);
    $profilePage->putHtml();
});

$router->get("/post/search", function ($in) {
    $searchPage = new CSearchPage();
    addMessagesToPageIfExists($searchPage, $in);
    $searchPage->putHtml();
});

// --------------------------------------------------------------------
// POST
// --------------------------------------------------------------------

$router->post("/user/login", function ($in) {
    global $router;

    if (!isset($in["uid"]) || !isset($in["pwd"])) {
        $router->redirect("/user/login", ["error" => CErrors::$err_missing_params]);
        exit();
    }

    $uid = filter_var(trim($in["uid"]), FILTER_SANITIZE_STRING);
    $pwd = filter_var(trim($in["pwd"]), FILTER_SANITIZE_STRING);

    if (($uid === false) || ($pwd === false) || ($uid !== $in["uid"]) || ($pwd !== $in["pwd"])) {
        $router->redirect("/user/login", ["error" => CErrors::$err_param_sanitazion]);
        exit();
    }

    $db = new CDatabaseConn();
    if (!$db->open()) {
        $router->redirect("/user/login", ["error" => $db->error]);
        exit();
    }

    if (!$db->validateCredentials($uid, $pwd)) {
        $router->redirect("/user/login", ["error" => $db->error]);
        exit();
    }

    CLoginManager::loginUser($uid);
    $router->redirect("/user/profile", ["uid" => CLoginManager::getCurrentUser()]);
});

$router->post("/user/logout", function ($in) {
    global $router;

    if (!CLoginManager::isUserLoggedIn()) {
        $router->redirect("/frontpage", ["error" => CErrors::$err_no_user_logged_in]);
        exit();
    }

    if (!isset($in["uid"])) {
        $router->redirect("/frontpage", ["error" => CErrors::$err_missing_uid]);
        exit();
    }

    if ($in["uid"] !== CLoginManager::getCurrentUser()) {
        $router->redirect("/frontpage", ["error" => CErrors::$err_no_user_logged_in]);
        exit();
    }

    CLoginManager::logoutUser();
    $router->redirect("/frontpage", ["msg" => "User logged out"]);
});

$router->post("/user/add", function ($in) {
    global $router;

    if (!isset($in["uid"]) || empty(trim($in["uid"]))) {
        $router->redirect("/user/add", ["error" => CErrors::$err_missing_uid]);
        exit();
    }

    if (!isset($in["email"]) || empty(trim($in["email"]))) {
        $router->redirect("/user/add", ["error" => CErrors::$err_missing_email]);
        exit();
    }

    if (filter_var(trim($in["email"]), FILTER_VALIDATE_EMAIL) === false) {
        $router->redirect("/user/add", ["error" => CErrors::$err_email_malformed]);
        exit();
    }

    if (!isset($in["pwd"]) || empty(trim($in["pwd"]))) {
        $router->redirect("/user/add", ["error" => CErrors::$err_missing_pwd]);
        exit();
    }

    if (!isset($in["pwd-rep"]) || empty(trim($in["pwd-rep"]))) {
        $router->redirect("/user/add", ["error" => CErrors::$err_missing_pwd_rep]);
        exit();
    }

    if ($in["pwd"] !== $in["pwd-rep"]) {
        $router->redirect("/user/add", ["error" => CErrors::$err_pwds_differ]);
        exit();
    }

    $uid = filter_var(trim($in["uid"]), FILTER_SANITIZE_STRING);
    $email = filter_var(trim($in["email"]), FILTER_SANITIZE_EMAIL);
    $pwd = filter_var(trim($in["pwd"]), FILTER_SANITIZE_STRING);

    if (($uid === false) || ($email === false) || ($pwd === false)) {
        $router->redirect("/user/add", ["error" => CErrors::$err_param_sanitazion]);
        exit();
    }

    // The credentials must match the given ones after sanitazion, to avoid
    // adding users that can later not log in
    if (($uid !== $in["uid"]) || ($email !== $in["email"]) || ($pwd !== $in["pwd"])) {
        $router->redirect("/user/add", ["error" => CErrors::$err_param_sanitazion]);
        exit();
    }

    $db = new CDatabaseConn();

    if (!$db->open()) {
        $router->redirect("/user/add", ["error" => $db->error]);
        exit();
    }

    if (!$db->createUser($uid, $pwd, $email)) {
        $router->redirect("/user/add", ["error" => $db->error]);
        exit();
    }

    $router->redirect("/user/add", ["msg" => "User created"]);
});

$router->post("/user/delete", function ($in) {
    echo "<p>user/delete router NOT IMPLEMENTED</p>";
});

$router->post("/post/add", function ($in) {
    global $router;

    // Only logged in users can comment other peoples posts and create posts on
    // their own profile
    if (!CLoginManager::isUserLoggedIn()) {
        $router->redirect("/frontpage", ["error" => CErrors::$err_no_user_logged_in]);
        exit();
    }

    if (!isset($in["title"]) || !isset($in["content"])) {
        $router->redirect("/frontpage", ["error" => CErrors::$err_missing_params]);
        exit();
    }

    $title = filter_var(trim($in["title"]), FILTER_SANITIZE_STRING);
    $content = filter_var(trim($in["content"]), FILTER_SANITIZE_STRING);

    if (($title === false) || ($content === false)) {
        $router->redirect("/frontpage", ["error" => CErrors::$err_param_sanitazion]);
        exit();
    }

    $parent = null;
    if (isset($in["parent"])) {
        $parent = filter_var($in["parent"], FILTER_SANITIZE_NUMBER_INT);
        if ($parent === false) {
            $router->redirect("/frontpage", ["error" => CErrors::$err_param_sanitazion]);
            exit();
        }
    }

    $db = new CDatabaseConn();

    if (!$db->open()) {
        $router->redirect("/frontpage", ["error" => $db->error]);
        exit();
    }

    if (!$db->createPost(CLoginManager::getCurrentUser(), $title, $content, $parent)) {
        $router->redirect("/frontpage", ["error" => $db->error]);
        exit();
    }

    // The post was made, return to my page
    $router->redirect("/user/profile", [
        "uid" => CLoginManager::getCurrentUser(),
        "msg" => "A new post was created by " . CLoginManager::getCurrentUser()
    ]);
});

$router->post("/post/delete", function ($in) {
    $router = CRouter::getInstance();

    if (!isset($in["uid"]) || !isset($in["postid"])) {
        $router->redirect("/frontpage", ["error" => CErrors::$err_missing_params]);
        exit();
    }

    // Only logged in users can delete posts
    if (!CLoginManager::isUserLoggedIn()) {
        $router->redirect("/frontpage", ["error" => CErrors::$err_no_user_logged_in]);
        exit();
    }

    $uid = filter_var(trim($in["uid"]), FILTER_SANITIZE_STRING);
    $postid = filter_var(trim($in["postid"]), FILTER_SANITIZE_STRING);

    if (($uid === false) || ($postid === false)) {
        $router->redirect("/frontpage", ["error" => CErrors::$err_param_sanitazion]);
        exit();
    }

    // One can only delete own posts
    if (CLoginManager::getCurrentUser() !== $in["uid"]) {
        $router->redirect("/frontpage", ["error" => CErrors::$err_invalid_operation]);
        exit();
    }

    $db = new CDatabaseConn();

    if ($db->open() === false) {
        $router->redirect("/frontpage", ["error" => $db->error]);
        exit();
    }

    if ($db->deletePost($postid, $uid) === false) {
        $router->redirect("/frontpage", ["error" => $db->error]);
        exit();
    }

    // The post was deleted, return to my page
    $router->redirect("/user/profile", [
        "uid" => CLoginManager::getCurrentUser(),
        "msg" => "Post " . $postid . " was deleted by " . $uid
    ]);
});

$router->post("/post/search", function ($in) {
    global $router;

    if (!isset($in["search-term"]) || empty(trim($in["search-term"]))) {
        $router->redirect("/post/search", ["error" => CErrors::$err_missing_params]);
        exit();
    }

    $searchTerm = filter_var(trim($in["search-term"]), FILTER_SANITIZE_STRING);
    if ($searchTerm === false) {
        $router->redirect("/post/search", ["error" => CErrors::$err_param_sanitazion]);
        exit();
    }

    $searchUser = null;
    if (isset($in["search-user"]) && !empty(trim($in["search-user"]))) {
        $searchUser = filter_var(trim($in["search-user"]), FILTER_SANITIZE_STRING);
        if ($searchUser === false) {
            $router->redirect("/post/search", ["error" => CErrors::$err_param_sanitazion]);
            exit();
        }
    }

    $searchResultsPage = new CSearchResultsPage($searchTerm, $searchUser);
    $searchResultsPage->putHtml();
});

$router->post("/post/upvote", function ($in) {
    global $router;

    if (!isset($in["uid"]) || empty(trim($in["uid"]))) {
        $router->redirect("/frontpage", ["error" => CErrors::$err_missing_params]);
        exit();
    }

    if (!isset($in["postid"]) || empty(trim($in["postid"]))) {
        $router->redirect("/frontpage", ["error" => CErrors::$err_missing_params]);
        exit();
    }

    $uid = filter_var(trim($in["uid"]), FILTER_SANITIZE_STRING);
    if ($uid === false) {
        $router->redirect("/frontpage", ["error" => CErrors::$err_param_sanitazion]);
        exit();
    }

    $postid = filter_var(trim($in["postid"]), FILTER_SANITIZE_NUMBER_INT);
    if ($postid === false) {
        $router->redirect("/frontpage", ["error" => CErrors::$err_param_sanitazion]);
        exit();
    }

    // Only logged in users can vote on posts
    if (!CLoginManager::isUserLoggedIn()) {
        $router->redirect("/frontpage", ["error" => CErrors::$err_no_user_logged_in]);
        exit();
    }

    // Note that this implementation let users vote on their own posts too.
    // A vote can go from -1 to 1. Where -1 is downvoted and 1 is upvoted.

    $db = new CDatabaseConn();

    if ($db->open() === false) {
        $router->redirect("/frontpage", ["error" => $db->error]);
        exit();
    }

    if ($db->upvotePost($uid, (int)$postid) === false) {
        $router->redirect("/frontpage", ["error" => $db->error]);
        exit();
    }

    // The post was upvoted
    $router->redirect("/frontpage", ["msg" => "Post " . $postid . " was upvoted by " . $uid]);
});

$router->post("/post/downvote", function ($in) {
    global $router;

    if (!isset($in["uid"]) || empty(trim($in["uid"]))) {
        $router->redirect("/frontpage", ["error" => CErrors::$err_missing_params]);
        exit();
    }

    if (!isset($in["postid"]) || empty(trim($in["postid"]))) {
        $router->redirect("/frontpage", ["error" => CErrors::$err_missing_params]);
        exit();
    }

    $uid = filter_var(trim($in["uid"]), FILTER_SANITIZE_STRING);
    if ($uid === false) {
        $router->redirect("/frontpage", ["error" => CErrors::$err_param_sanitazion]);
        exit();
    }

    $postid = filter_var(trim($in["postid"]), FILTER_SANITIZE_NUMBER_INT);
    if ($postid === false) {
        $router->redirect("/frontpage", ["error" => CErrors::$err_param_sanitazion]);
        exit();
    }

    // Only logged in users can vote on posts
    if (!CLoginManager::isUserLoggedIn()) {
        $router->redirect("/frontpage", ["error" => CErrors::$err_no_user_logged_in]);
        exit();
    }

    // Note that this implementation let users vote on their own posts too.
    // A vote can go from -1 to 1. Where -1 is downvoted and 1 is upvoted.

    $db = new CDatabaseConn();

    if ($db->open() === false) {
        $router->redirect("/frontpage", ["error" => $db->error]);
        exit();
    }

    if ($db->downvotePost($uid, (int)$postid) === false) {
        $router->redirect("/frontpage", ["error" => $db->error]);
        exit();
    }

    // The post was downvoted
    $router->redirect("/frontpage", ["msg" => "Post " . $postid . " was downvoted by " . $uid]);
});

$router->resolve();
