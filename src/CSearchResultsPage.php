<?php

declare(strict_types=1);

namespace myforum\private;

require_once __DIR__ . "/CMessages.php";
require_once __DIR__ . "/CHeader.php";
require_once __DIR__ . "/CFooter.php";
require_once __DIR__ . "/CDatabaseConn.php";
require_once __DIR__ . "/CPostList.php";

class CSearchResultsPage extends CMessages
{
    private string | null $searchTerm;
    private string | null $searchUser;

    public function __construct(
        string $searchTerm = null,
        string $searchUser = null,
    ) {
        $this->searchTerm = $searchTerm;
        $this->searchUser = $searchUser;
    }

    private function putSearchResults()
    {
        if (!is_null($this->searchTerm) && !empty($this->searchTerm)) {

            $db = new CDatabaseConn();

            if ($db->open() === false) {
                CRouter::getInstance()->redirect("/post/search", ["error" => $db->error]);
                exit();
            }

            $posts = [];

            if (!is_null($this->searchUser) && !empty($this->searchUser)) {
                $posts = $db->findPostsByUser($this->searchTerm, $this->searchUser);
                if ($posts === false) {
                    CRouter::getInstance()->redirect("/post/search", ["error" => $db->error]);
                    exit();
                }
            } else {
                $posts = $db->findPosts($this->searchTerm);
                if ($posts === false) {
                    CRouter::getInstance()->redirect("/post/search", ["error" => $db->error]);
                    exit();
                }
            }

            $postCount = count($posts);
            if ($postCount > 0) {
                if (is_null($this->searchUser)) {
                    echo "<p><em>{$postCount} posts match \"{$this->searchTerm}\"</em></p>";
                } else {
                    echo "<p><em>{$postCount} posts match \"{$this->searchTerm}\" by {$this->searchUser}</em></p>";
                }

                $postList = new CPostList($posts);
                $postList->putHtml();
            } else {
                echo "<p><em>No posts found ...</em></p>";
            }
        }
    }

    public function putHtml()
    {
        $header = new CHeader("MyForum - Search results");
        $header->putHtml();

        echo "<main>";

        CMessages::putHtml();

        $this->putSearchResults();

        echo "</main>";

        $footer = new CFooter();
        $footer->putHtml();
    }
}
