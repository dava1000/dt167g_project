<?php

declare(strict_types=1);

namespace myforum\private;

require_once __DIR__ . "/CMessages.php";
require_once __DIR__ . "/CHeader.php";
require_once __DIR__ . "/CFooter.php";
require_once __DIR__ . "/CLoginForm.php";

class CLoginPage extends CMessages
{
    public function putHtml()
    {
        $header = new CHeader("MyForum - Login");
        $header->putHtml();

        echo "<main>";

        CMessages::putHtml();

        $loginForm = new CLoginForm();
        $loginForm->putHtml();

        echo "</main>";

        $footer = new CFooter();
        $footer->putHtml();
    }
}
