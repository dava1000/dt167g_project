<?php

declare(strict_types=1);

namespace myforum\private;

require_once __DIR__ . "/CTokenManager.php";
require_once __DIR__ . "/IHtmlOutput.php";

class CLoginForm implements IHtmlOutput
{
    public function putHtml()
    {
        [$tk, $ts] = CTokenManager::generate();

        echo "
        <form action='/user/login' method='POST'>
            <fieldset>
                <legend>Credentials</legend>
                <input type='text' name='uid' id='uid' , placeholder='username ...' required>
                <input type='password' name='pwd' id='pwd' placeholder='password ...' required>
                <input type='submit' id='submit' value='Login'>
            </fieldset>
            <input type='hidden' name='tk' value='{$tk}' />
            <input type='hidden' name='ts' value='{$ts}' />
        </form>";
    }
}
