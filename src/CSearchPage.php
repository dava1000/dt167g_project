<?php

declare(strict_types=1);

namespace myforum\private;

require_once __DIR__ . "/CMessages.php";
require_once __DIR__ . "/CHeader.php";
require_once __DIR__ . "/CFooter.php";
require_once __DIR__ . "/CSearchForm.php";

class CSearchPage extends CMessages
{
    public function putHtml()
    {
        $header = new CHeader("MyForum - Search");
        $header->putHtml();

        echo "<main>";

        CMessages::putHtml();

        $searchForm = new CSearchForm();
        $searchForm->putHtml();

        echo "</main>";

        $footer = new CFooter();
        $footer->putHtml();
    }
}
