# README

This is my implementation of a safe forum-like website for the course
DT167G. It features countermeasures to the following kinds of attacks:

1. CSRF
2. XSS
3. SQL-Injection

The website is implemented in PHP and hosted on a linux virtual machine.
An apache server is used to serve the contents of the website.
