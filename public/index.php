<?php

namespace myforum\public;

ini_set("default_charset", "utf-8");
ini_set("mbstring.internal_encoding", "utf-8");
ini_set("mbstring.http_output=", "UTF-8");
ini_set("mbstring.encoding_translation", "On");
ini_set("mbstring.func_overload", "6");

require_once __DIR__ . "/../src/routes.php";
