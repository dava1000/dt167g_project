<?php

declare(strict_types=1);

namespace myforum\private;

# the site secret value
$site_secret = "a strong and secure secret";

# set to release to make error messages from database generic
$env_debug = "debug";
$env_release = "release";
$env = $env_debug;

# mariadb configuration
$dbconfig = [
    "DB_HOST" => "localhost",
    "DB_PORT" => 3306,
    "DB_DB" => "myforum",
    "DB_USER" => "myforum",
    "DB_PWD" => "a strong and complicated password"
];
