-- File   db_schema.sql
-- Author dava1000

CREATE DATABASE myforum CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE myforum;

CREATE TABLE User (
  username VARCHAR(64) NOT NULL,
  email VARCHAR(64) NOT NULL UNIQUE,
  password VARCHAR(256) NOT NULL,
  reg_date timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (username)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE Post (
  parent INT DEFAULT NULL,
  id INT NOT NULL AUTO_INCREMENT,
  user varchar(64) NOT NULL,
  date TIMESTAMP NOT NULL DEFAULT current_timestamp(),
  title VARCHAR(120) NOT NULL,
  content VARCHAR(1024) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_post_user FOREIGN KEY (user) REFERENCES User (username)
    ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT fk_post_parent FOREIGN KEY (parent) REFERENCES Post (id)
    ON UPDATE CASCADE ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE Voting (
  user VARCHAR(64) NOT NULL,
  post INT NOT NULL,
  vote INT NOT NULL DEFAULT 0,
  PRIMARY KEY (user, post),
  CONSTRAINT fk_voting_user FOREIGN KEY (user) REFERENCES User (username)
    ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT fk_voting_post FOREIGN KEY (post) REFERENCES Post (id)
    ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
